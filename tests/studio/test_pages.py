"""
Acceptance tests for Studio.
"""
from bok_choy.web_app_test import WebAppTest

from bharatmoocapp_pages.studio.asset_index import AssetIndexPage
from bharatmoocapp_pages.studio.checklists import ChecklistsPage
from bharatmoocapp_pages.studio.course_import import ImportPage
from bharatmoocapp_pages.studio.course_info import CourseUpdatesPage
from bharatmoocapp_pages.studio.edit_tabs import PagesPage
from bharatmoocapp_pages.studio.export import ExportPage
from bharatmoocapp_pages.studio.howitworks import HowitworksPage
from bharatmoocapp_pages.studio.index import DashboardPage
from bharatmoocapp_pages.studio.login import LoginPage
from bharatmoocapp_pages.studio.manage_users import CourseTeamPage
from bharatmoocapp_pages.studio.overview import CourseOutlinePage
from bharatmoocapp_pages.studio.settings import SettingsPage
from bharatmoocapp_pages.studio.settings_advanced import AdvancedSettingsPage
from bharatmoocapp_pages.studio.settings_graders import GradingPage
from bharatmoocapp_pages.studio.signup import SignupPage
from bharatmoocapp_pages.studio.textbooks import TextbooksPage

from ..helpers import visit_all


class PagesTest(WebAppTest):
    """
    Smoke test that we can visit pages in Studio.
    """

    # We use the global staff user to log in, because we know they will have access
    # to Studio.  This is not ideal, and longer term we should install and test with
    # instructor/course-staff users instead.
    STUDIO_USER = "staff@example.com"
    STUDIO_PASSWORD = "bharatmooc"

    DEMO_COURSE_INFO = ('BharatMOOC', 'Open_DemoX', 'bharatmooc_demo_course')

    def test_logged_out_pages(self):
        visit_all([
            clz(self.browser) for clz in [LoginPage, HowitworksPage, SignupPage]
        ])

    def test_logged_in_pages(self):

        # Log in to Studio
        login_page = LoginPage(self.browser)
        login_page.visit()
        login_page.login(self.STUDIO_USER, self.STUDIO_PASSWORD)

        # Check that we can get to the dashboard
        DashboardPage(self.browser).wait_for_page()

        # Check that we can get to course editing pages
        visit_all([
            clz(self.browser, *self.DEMO_COURSE_INFO) for clz in
            [AssetIndexPage, ChecklistsPage, ImportPage, CourseUpdatesPage,
            PagesPage, ExportPage, CourseTeamPage, CourseOutlinePage, SettingsPage,
            AdvancedSettingsPage, GradingPage, TextbooksPage]])
